import requests
from bs4 import BeautifulSoup
import pandas as pd

format = input("Enter your format: ")
content = input("Enter your content: ")
type_content = input("Enter your type content:")
URL = f'https://www.datosabiertos.gob.pe/search/field_resources%253Afield_format/{format}/field_topic/{content}/type/{type_content}?query=donaciones&sort_by=changed&sort_order=DESC'
page = requests.get(URL)
soup = BeautifulSoup(page.text, "html.parser")
list_urls = [i for i in soup.find_all('a', {'class': "label"})]
first = str(list_urls[0]).split('href="')[1]
second = first.split('" title')[0]
base_url = 'https://www.datosabiertos.gob.pe/'
url_complete = base_url + second
page = requests.get(url_complete)
soup = BeautifulSoup(page.text, "html.parser")
list_urls = [i for i in soup.find_all('a', {'class': "data-link"})]
first = str(list_urls[2]).split('href="')[1]
second = first.split('"><i')[0]
r = requests.get(second, allow_redirects=True)
open('donaciones.zip', 'wb').write(r.content)

df = pd.read_csv('donaciones.zip', encoding="ISO-8859-1")
region_list = df.REGION.unique()
for i in region_list:
    data_per_region = df.loc[df['REGION'] == i]
    data_per_region.to_csv(f'{i}.csv')
